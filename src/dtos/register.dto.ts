import { ApiProperty } from "@nestjs/swagger";
import { RoleEnum } from "../enums/role.enum";

export class RegisterDTO {
    @ApiProperty({ name: "email", example: "jdoe@mail.com" })
    email: string;
    @ApiProperty({ name: "password" })
    password: string;
    @ApiProperty({ name: "roles", enum: RoleEnum })
    roles: RoleEnum[];
    @ApiProperty({ name: "username", example: "johndoe" })
    username: string;
}
