import { ApiProperty } from "@nestjs/swagger";
import { TicketStatusesEnum } from "../enums/statuses.enum";

export class TicketDTO {
    @ApiProperty({ name: "_id", example: "62cd5d37314d1125018be763" })
    _id?: string;
    @ApiProperty({ name: "title", example: "Create React App" })
    title: string;
    @ApiProperty({ name: "reference", example: "CRA1" })
    reference: string;
    @ApiProperty({
        name: "content",
        example: "My first ticket",
        required: false
    })
    content: string;
    @ApiProperty({ name: "creator", example: "johndoe" })
    creator: string;
    @ApiProperty({
        name: "status",
        enum: TicketStatusesEnum,
        default: TicketStatusesEnum.NEW
    })
    status: TicketStatusesEnum;
}
