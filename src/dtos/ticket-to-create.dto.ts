import { ApiProperty } from "@nestjs/swagger";
import { TicketStatusesEnum } from "../enums/statuses.enum";

export class TicketToCreateDTO {
    @ApiProperty({ name: "title", example: "Create React App" })
    title: string;
    @ApiProperty({
        name: "reference",
        example: "CRA1",
        description: "This value must be unique"
    })
    reference: string;
    @ApiProperty({
        name: "content",
        example: "My first ticket",
        required: false
    })
    content: string;
    @ApiProperty({
        name: "status",
        enum: TicketStatusesEnum,
        default: TicketStatusesEnum.USER_STORY,
        required: false
    })
    status: TicketStatusesEnum;
}
