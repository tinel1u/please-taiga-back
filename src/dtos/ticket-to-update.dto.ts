import { ApiProperty } from "@nestjs/swagger";
import { TicketStatusesEnum } from "../enums/statuses.enum";

export class TicketToUpdateDTO {
    @ApiProperty({
        name: "title",
        example: "Create React App",
        required: false
    })
    title: string;
    @ApiProperty({ name: "reference", example: "CRA1", required: false })
    reference: string;
    @ApiProperty({
        name: "content",
        example: "My first ticket",
        required: false
    })
    content: string;
    @ApiProperty({ name: "creator", example: "johndoe", required: false })
    creator: string;
    @ApiProperty({ name: "status", enum: TicketStatusesEnum, required: false })
    status: TicketStatusesEnum;
}
