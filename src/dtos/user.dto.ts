import { ApiProperty } from "@nestjs/swagger";
import { RoleEnum } from "../enums/role.enum";

export class UserDTO {
    @ApiProperty({
        name: "email",
        example: "johndoe@mail.com",
        description: "This value must be unique"
    })
    email: string;
    @ApiProperty({ name: "password" })
    password: string;
    @ApiProperty({ name: "role", enum: RoleEnum })
    role: RoleEnum[];
    @ApiProperty({
        name: "username",
        example: "johndoe",
        description: "This value must be unique"
    })
    username: string;
}
