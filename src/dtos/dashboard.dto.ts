import { ApiProperty } from "@nestjs/swagger";

export class DashboardDTO {
    @ApiProperty({ name: "user_story", example: 1 })
    user_story: number;
    @ApiProperty({ name: "new", example: 1 })
    new: number;
    @ApiProperty({ name: "in_progress", example: 3 })
    in_progress: number;
    @ApiProperty({ name: "ready_for_test", example: 1 })
    ready_for_test: number;
    @ApiProperty({ name: "closed", example: 0 })
    closed: number;
    @ApiProperty({ name: "needs_info", example: 1 })
    needs_info: number;
}
