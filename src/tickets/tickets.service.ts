import { Injectable } from "@nestjs/common";
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { Ticket } from "../interfaces/ticket.interface";
import { TicketToCreateDTO } from "../dtos/ticket-to-create.dto";
import { TicketToUpdateDTO } from "../dtos/ticket-to-update.dto";
import { TicketStatusesEnum } from "../enums/statuses.enum";

@Injectable()
export class TicketsService {
    constructor(
        // eslint-disable-next-line no-unused-vars
        @InjectModel("Ticket") private readonly ticketModel: Model<Ticket>
    ) {}

    async create(
        username: string,
        createTicketDTO: TicketToCreateDTO
    ): Promise<Ticket> {
        const newTicket = await new this.ticketModel({
            ...createTicketDTO,
            creator: username
        });
        return Object.values(TicketStatusesEnum).includes(
            createTicketDTO.status
        )
            ? newTicket.save()
            : undefined;
    }

    async delete(_id): Promise<boolean> {
        return await this.ticketModel.findByIdAndRemove(_id);
    }

    async getAll(): Promise<Ticket[]> {
        const tickets = await this.ticketModel.find().exec();
        return tickets;
    }

    async getById(_id): Promise<Ticket> {
        return await this.ticketModel.findById(_id).exec();
    }

    async getByReference(reference): Promise<Ticket> {
        const ticket = await this.ticketModel
            .findOne({ reference: reference })
            .exec();

        return ticket;
    }

    async update(_id, updateTicketDTO: TicketToUpdateDTO): Promise<boolean> {
        const ticket = await this.ticketModel.findByIdAndUpdate(
            _id,
            updateTicketDTO,
            { new: true }
        );
        return ticket !== undefined;
    }
}
