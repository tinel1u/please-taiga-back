import * as mongoose from "mongoose";
import { TicketStatusesEnum } from "src/enums/statuses.enum";
const { Schema } = mongoose;

export const TicketSchema = new Schema({
    title: { type: String, unique: false, required: false },
    reference: { type: String, unique: true, required: true },
    content: { type: String, unique: false, required: false },
    creator: { type: String, unique: false, required: true },
    status: {
        type: String,
        enum: TicketStatusesEnum
    }
});
