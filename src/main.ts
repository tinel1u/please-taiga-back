import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";

import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import { TicketModule } from "./tickets/ticket.module";
import { UserModule } from "./user/user.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { AuthModule } from "./auth/auth.module";

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.enableCors({
        origin: "*",
        allowedHeaders: [
            "Content-Type",
            "Authorization",
            "X-Requested-With",
            "Accept"
        ],
        methods: "GET, POST, PUT, DELETE, OPTIONS"
    });
    app.setGlobalPrefix("api/v1");

    const options = new DocumentBuilder()
        .setTitle("Taiga ticketing app")
        .setDescription("A documentation for tickets")
        .setVersion("1.0")
        .addTag("Authentication")
        .addTag("Dashboard")
        .addTag("Tickets")
        .addTag("Users")
        .addBearerAuth()
        .build();
    const appDocument = SwaggerModule.createDocument(app, options, {
        include: [AuthModule, DashboardModule, TicketModule, UserModule]
    });
    SwaggerModule.setup("api", app, appDocument);
    await app.listen(3000);
}
bootstrap();
