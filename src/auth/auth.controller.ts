import {
    Body,
    Controller,
    Get,
    HttpStatus,
    Param,
    Post,
    Res
} from "@nestjs/common";
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiForbiddenResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiTags,
    ApiUnauthorizedResponse
} from "@nestjs/swagger";
import { LoginDTO } from "../dtos/login.dto";
import { RegisterDTO } from "../dtos/register.dto";
import { UserService } from "../user/user.service";
import { AuthService } from "./auth.service";
import { Roles } from "./roles/role.decorator";
import { RoleEnum } from "../enums/role.enum";

@ApiTags("Authentication")
@ApiBearerAuth()
@Controller("auth")
export class AuthController {
    constructor(
        // eslint-disable-next-line no-unused-vars
        private userService: UserService,
        // eslint-disable-next-line no-unused-vars
        private authService: AuthService
    ) {}

    @ApiOkResponse({ status: 200, description: "Token found", type: String })
    @ApiNotFoundResponse({ status: 404, description: "Token not found" })
    @ApiUnauthorizedResponse({ status: 401, description: "Unauthorized" })
    @ApiForbiddenResponse({
        status: 403,
        description: "Forbidden ('Admin' role required)"
    })
    @Get("/token/:username")
    @Roles(RoleEnum.ADMIN)
    async getToken(@Res() res, @Param("username") username: string) {
        const user = await this.userService.findByUsername(username);

        const payload = {
            email: user.email
        };

        const token = await this.authService.signPayload(payload);

        return token === undefined
            ? res.status(HttpStatus.NOT_FOUND).json()
            : res.status(HttpStatus.OK).json({
                  status: 200,
                  data: token
              });
    }

    @ApiOkResponse({
        status: 200,
        description: "Registration successful",
        type: String
    })
    @ApiBadRequestResponse({ status: 400, description: "Bad request" })
    @ApiUnauthorizedResponse({ status: 401, description: "Unauthorized" })
    @ApiForbiddenResponse({ status: 403, description: "Forbidden" })
    @Post("register")
    async register(@Res() res, @Body() registerDTO: RegisterDTO) {
        if (
            registerDTO?.email === undefined ||
            registerDTO?.password === undefined ||
            registerDTO?.roles === undefined ||
            registerDTO?.username === undefined
        ) {
            return res.status(HttpStatus.BAD_REQUEST).json();
        }

        if (
            (await this.userService.findByUsername(registerDTO?.username)) ||
            (await this.userService.findByEmail(registerDTO?.email))
        ) {
            return res.status(HttpStatus.CONFLICT).json();
        }

        const createdUser = await this.userService.create(registerDTO);
        const payload = {
            email: createdUser.email
        };

        const token = await this.authService.signPayload(payload);

        return res.status(HttpStatus.OK).json({
            data: { createdUser, token }
        });
    }

    @ApiOkResponse({
        status: 200,
        description: "Login successful",
        type: String
    })
    @ApiNotFoundResponse({ status: 404, description: "User not found" })
    @Post("login")
    async login(@Res() res, @Body() loginDTO: LoginDTO) {
        const user = await this.userService.findByLogin(loginDTO);
        const payload = {
            email: user.email
        };
        const token = await this.authService.signPayload(payload);
        return user && token
            ? res.status(HttpStatus.OK).json({
                  token: token,
                  roles: user.roles
              })
            : res.status(HttpStatus.NOT_FOUND).json();
    }
}
