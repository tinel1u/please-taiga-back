/* eslint-disable no-unused-vars */
export enum TicketStatusesEnum {
    USER_STORY = "user story",
    NEW = "new",
    IN_PROGRESS = "in progress",
    READY_FOR_TEST = "ready for test",
    CLOSED = "closed",
    NEEDS_INFO = "needs info"
}
