## Description

A taiga ticketing like REST Api built with Nest framework. (When server running, autogenerated Swagger available at http://localhost:3000/api#/)

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

-   Author - [Adrien Tinel]
